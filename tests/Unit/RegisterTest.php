<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic register test.
     *
     * @return void
     */
    public function test_register_is_successful()
    {
        $user = User::factory()->make();

        $payload = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'dni' => $user->dni,
            'phone' => $user->phone,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $response = $this->post('/register', $payload);
        $response->assertStatus(HttpResponse::HTTP_FOUND);
        $response->assertRedirect('/home');

        $registered = User::query()->where('email', $user->email)->firstOrFail();
        $this->assertModelExists($registered);

    }

    /**
     * A basic register test.
     *
     * @return void
     */
    public function test_register_is_wrong_response()
    {
        $user = User::factory()->make();

        $payload = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'dni' => $user->dni,
            'phone' => $user->phone,
            'email' => 'fakeemail',
            'password' => 'password',
            'password_confirmation' => 'passwod'
        ];

        $response = $this->post('/register', $payload);
        $response->assertStatus(HttpResponse::HTTP_FOUND);
        $response->assertSessionHasErrors(['password', 'email']);
    }
}
