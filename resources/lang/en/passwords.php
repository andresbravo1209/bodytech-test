<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Tu contraseña ha sido reiniciada!',
    'sent' => 'Hemos enviado un link de restablecimiento a tu correo electrónico!',
    'throttled' => 'Por favor espere, para reintentar',
    'token' => 'Este token para cambio de contraseña es invalido.',
    'user' => "No se encuentra un usuario con esta dirección de correo electrónico",

];
