<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function __invoke(LoginRequest $request): JsonResponse
    {
        $user = User::query()->where('email', $request->get('email'))->first();

        if (! $user || ! Hash::check($request->get('password'), $user->password)) {
            return response()->error([
                'message' => 'Credenciales invalidas.',
                'error' => 'Correo electronico o contraseña invalidas.',
            ], HttpResponse::HTTP_BAD_REQUEST);
        }

        return response()->success([
            'token' => $user->createToken('token')->plainTextToken,
        ]);
    }
}
