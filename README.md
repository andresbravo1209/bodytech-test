## Prueba Tecnica de Conocimiento Bodytech.

#### Desarrollado por: Andres Bravo
#### Laravel 8^ - MySQL.

1. Clonar y configurar credenciales del repositorio.
2. Copiar y configurar archivo .env.example en el archivo de configuración del proyecto.
3. Configurar credenciales de MAILTRAP ó solicitar clave de acceso al administrador del repositorio.
4. Instalación de dependencias frontend utilzando NODE y Laravel MIX

- npm install
- npm run dev

5. Correr migraciones para obtener nuestra tabla de usuarios.

- php artisan migrate


6. Obtener datos de prueba

- php artisan db:seed --class=DatabaseSeeder

*Datos de prueba, contraseña por defecto "password"*
  
7. Ejecución de pruebas unitarias.

*Para el correcto funcionamiento de las pruebas unitarias es 
necesario instanciar un ambiente de pruebas o DB para ellas, nombrada bodytech-test [revisar archivo de configuración phpunit.xml]*
   
- php artisan test | php artisan test --filter LoginTest | php artisan test --filter RegisterTest 
